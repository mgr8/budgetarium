// For authoring Nightwatch tests, see
// http://nightwatchjs.org/guide#usage

module.exports = {
  'AccountsListView Test': function (browser) {
    // automatically uses dev Server port from /config.index.js
    // default: http://localhost:8080
    // see nightwatch.conf.js
    const devServer = browser.globals.devServerURL;

    browser
      .url(devServer + '/')
      .waitForElementVisible('#app', 5000)
      .assert.elementPresent('#accounts-list-view')
      .assert.containsText('div', 'I\'m a list of accounts!')
      .assert.elementCount('img', 1)
      .end();
  },
  'BudgetsListView Test': function (browser) {
    // automatically uses dev Server port from /config.index.js
    // default: http://localhost:8080
    // see nightwatch.conf.js
    const devServer = browser.globals.devServerURL;

    browser
      .url(devServer + '/budgets')
      .waitForElementVisible('#app', 5000)
      .assert.elementPresent('#budgets-list-view')
      .assert.containsText('div', 'I\'m a list of budgets!')
      .assert.elementCount('img', 1)
      .end();
  },
  'TransactionsListView Test': function (browser) {
    // automatically uses dev Server port from /config.index.js
    // default: http://localhost:8080
    // see nightwatch.conf.js
    const devServer = browser.globals.devServerURL;

    browser
      .url(devServer + '/transactions')
      .waitForElementVisible('#app', 5000)
      .assert.elementPresent('#transactions-list-view')
      .assert.containsText('div', 'I\'m a list of transactions!')
      .assert.elementCount('img', 1)
      .end();
  }
};

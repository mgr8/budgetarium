// import Vue from 'vue';
import AccountsListView from '../../../src/app/accounts/components/AccountsListView.vue';
import { getComponent } from '../test_utils';

describe('AccountsListView.vue', () => {
  // it('should be at "/"', () => {
  //   expect()
  // });
  it('should render correct contents', () => {
    expect(getComponent(AccountsListView).textContent.trim())
      .to.equal('I\'m a list of accounts!');
  });
});

// import Vue from 'vue';
import BudgetsListView from '../../../src/app/budgets/components/BudgetsListView.vue';
import { getComponent } from '../test_utils';

describe('BudgetsListView.vue', () => {
  it('should render correct contents', () => {
    expect(getComponent(BudgetsListView).textContent.trim())
      .to.equal('I\'m a list of budgets!');
  });
});

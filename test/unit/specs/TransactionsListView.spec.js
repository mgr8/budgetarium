// import Vue from 'vue';
import TransactionsListView from '../../../src/app/transactions/components/TransactionsListView.vue';
import { getComponent } from '../test_utils';

describe('AccountsListView.vue', () => {
  it('should render correct contents', () => {
    expect(getComponent(TransactionsListView).textContent.trim())
      .to.equal('I\'m a list of transactions!');
  });
});
